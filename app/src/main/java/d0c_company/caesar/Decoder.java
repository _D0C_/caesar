package d0c_company.caesar;

import android.support.annotation.StringDef;

/**
 * Created by D0C on 08.12.2016.
 */

public class Decoder {

    private char swap (char i) {

            boolean up = false;

            if (Character.isUpperCase(i)) up = true;

            i = Character.toLowerCase(i);
            if (i >= 'а' && i <= 'я' || i == 'ё') {
                switch (i) {
                case 'а':
                    i = 'б';
                    break;
                case 'б':
                    i = 'в';
                    break;
                case 'в':
                    i = 'г';
                    break;
                case 'г':
                    i = 'д';
                    break;
                case 'д':
                    i = 'е';
                    break;
                case 'е':
                    i = 'ё';
                    break;
                case 'ё':
                    i = 'ж';
                    break;
                case 'ж':
                    i = 'з';
                    break;
                case 'з':
                    i = 'и';
                    break;
                case 'и':
                    i = 'й';
                    break;
                case 'й':
                    i = 'к';
                    break;
                case 'к':
                    i = 'л';
                    break;
                case 'л':
                    i = 'м';
                    break;
                case 'м':
                    i = 'н';
                    break;
                case 'н':
                    i = 'о';
                    break;
                case 'о':
                    i = 'п';
                    break;
                case 'п':
                    i = 'р';
                    break;
                case 'р':
                    i = 'с';
                    break;
                case 'с':
                    i = 'т';
                    break;
                case 'т':
                    i = 'у';
                    break;
                case 'у':
                    i = 'ф';
                    break;
                case 'ф':
                    i = 'х';
                    break;
                case 'х':
                    i = 'ц';
                    break;
                case 'ц':
                    i = 'ч';
                    break;
                case 'ч':
                    i = 'ш';
                    break;
                case 'ш':
                    i = 'щ';
                    break;
                case 'щ':
                    i = 'ъ';
                    break;
                case 'ъ':
                    i = 'ы';
                    break;
                case 'ы':
                    i = 'ь';
                    break;
                case 'ь':
                    i = 'э';
                    break;
                case 'э':
                    i = 'ю';
                    break;
                case 'ю':
                    i = 'я';
                    break;
                case 'я':
                    i = 'а';
                    break;
            }
            }
        if (up) i = Character.toUpperCase(i);

        return (i);
    }

    public String decode(String text) {
        String a = "";
        for (int i = 0; i < text.length(); i++) {
            a += swap(text.charAt(i));
        }

        return a;
    }
    public String Decode(String text,int k){
        for (int i =0; i<k;i++){
            text = decode(text);
        }
        return text;
    }
}
