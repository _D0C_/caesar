package d0c_company.caesar;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static d0c_company.caesar.R.id.input;
import static d0c_company.caesar.R.id.outputLayout;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    Button button,eOutput;
    TextView out, decode;
    EditText eInput;
    Decoder a = new Decoder();
    String data[] = {"ROT1","ROT2","ROT3","ROT4","ROT5","ROT6","ROT7","ROT8","ROT9","ROT10","ROT11","ROT12","ROT13","ROT14","ROT15","ROT16","ROT17","ROT18","ROT19","ROT20","ROT21","ROT22","ROT23","ROT24","ROT25","ROT26","ROT27","ROT28","ROT29","ROT30","ROT31","ROT32"};
    String input;
    int k;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        out =(TextView) findViewById(R.id.outText) ;
        eInput =(EditText) findViewById(R.id.input);
        eOutput=(Button) findViewById(R.id.output);
        button = (Button) findViewById(R.id.button);
        spinner = (Spinner) findViewById(R.id.spinner);
        decode = (TextView) findViewById(R.id.decodeText);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setPrompt("Ключ");
        spinner.setSelection(0);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                k = position+1;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = eInput.getText().toString();
                eOutput.setText(a.Decode(input,k));
                out.setVisibility(View.VISIBLE);
                eOutput.setVisibility(View.VISIBLE);
                decode.setText("Для обратного преобразования используйте ключ " + data[32-k]);
            }
        });
        eOutput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", eOutput.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast toast = Toast.makeText(getApplicationContext(),"Текст скопирован в буфер обмена",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }


}
